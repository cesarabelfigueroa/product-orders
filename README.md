## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Running the app with docker/docker compose 
```bash
$  docker-compose up --build
```

## Test
Please check the folder called /test into the project, there are collections from postman that can be used
to test the api.