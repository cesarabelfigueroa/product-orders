import { Controller, Get, Post, Body, Param, UseGuards } from '@nestjs/common';
import { ProductsService } from './product.service';
import { Product } from './product.schema';
import { AuthGuard } from '@nestjs/passport';


@Controller('products')
@UseGuards(AuthGuard('jwt'))
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Post()
  async create(@Body() createProductDto: any): Promise<{ data: Product }> {
    const product = await this.productsService.create(createProductDto);
    return { data: product };
  }

  @Get()
  async findAll(): Promise<{ data: Product[] }> {
    const products = await this.productsService.findAll();
    return { data: products };
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<{ data: Product }> {
    const product = await this.productsService.findOne(id);
    return { data: product };
  }
}
