import { Injectable } from '@nestjs/common';
import { Express } from 'express';
import { promises as fs } from 'fs';
import { join } from 'path';

@Injectable()
export class FileUploadService {
  async uploadFile(file: Express.Multer.File) {
    const uploadDir = join(__dirname, '..', 'uploads');
    const uploadPath = join(uploadDir, file.originalname);

    await this.ensureDir(uploadDir);
    await fs.writeFile(uploadPath, file.buffer);
    return {
      message: 'File uploaded successfully',
      fileName: file.originalname,
    };
  }

  private async ensureDir(dir: string) {
    try {
      await fs.access(dir);
    } catch (err) {
      await fs.mkdir(dir, { recursive: true });
    }
  }
}
