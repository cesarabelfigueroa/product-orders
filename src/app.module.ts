import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { ProductsModule } from './products/products.module';
import { OrdersModule } from './orders/order.module';
import { PassportModule } from '@nestjs/passport';
import { AuthModule } from './auth/auth.module';
import { FileUploadModule } from  './file-upload/file-upload.module'


@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot(process.env.MONGO_URI),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    ProductsModule,
    OrdersModule,
    AuthModule,
    FileUploadModule
  ],
  controllers: [AppController],
  providers: [AppService],
})


export class AppModule {}