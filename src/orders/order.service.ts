import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Order } from './order.schema';
import { Product } from '../products/product.schema';

@Injectable()
export class OrdersService {
  constructor(
    @InjectModel(Order.name) private orderModel: Model<Order>,
    @InjectModel(Product.name) private productModel: Model<Product>,
  ) {}

  async create(createOrderDto: any): Promise<Order> {
    const total = await this.calculateTotal(createOrderDto.productList);
    const createdOrder = new this.orderModel({ ...createOrderDto, total });
    return createdOrder.save();
  }

  async update(id: string, updateOrderDto: any): Promise<Order> {
    const total = await this.calculateTotal(updateOrderDto.productList);
    return this.orderModel
      .findByIdAndUpdate(id, { ...updateOrderDto, total }, { new: true })
      .exec();
  }

  async findAll(): Promise<Order[]> {
    return this.orderModel.find().exec();
  }

  async findOne(id: string): Promise<Order> {
    return this.orderModel.findById(id).exec();
  }

  async getTotalSoldLastMonth(): Promise<number> {
    const now = new Date();

    const startOfLastMonth = new Date(now.getFullYear(), now.getMonth() - 1, 1);
    const startOfThisMonth = new Date(now.getFullYear(), now.getMonth(), 1);

    const orders = await this.orderModel
      .find({
        createdAt: { $gte: startOfLastMonth, $lt: startOfThisMonth },
      })
      .exec();
      
    return orders.reduce((total, order) => total + order.total, 0);
  }

  async getHighestAmountOrder(): Promise<Order> {
    return this.orderModel.findOne().sort({ total: -1 }).exec();
  }

  private async calculateTotal(productList: string[]): Promise<number> {
    const products = await this.productModel
      .find({ _id: { $in: productList } })
      .exec();
    return products.reduce((sum, product) => sum + product.price, 0);
  }
}
