import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

@Schema({ timestamps: true }) 
export class Order extends Document {


  @Prop({ required: true })
  clientName: string;

  @Prop({ required: true })
  total: number;

  @Prop({ type: [{ type: Types.ObjectId, ref: 'Product' }], required: true })
  productList: Types.Array<Types.ObjectId>;
}

export const OrderSchema = SchemaFactory.createForClass(Order);