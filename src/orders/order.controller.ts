import { Controller, Get, Post, Put, Param, Body, UseGuards } from '@nestjs/common';
import { OrdersService } from './order.service';
import { Order } from './order.schema';
import { AuthGuard } from '@nestjs/passport';

@Controller('orders')
@UseGuards(AuthGuard('jwt'))
export class OrdersController {
  constructor(private readonly ordersService: OrdersService) {}

  @Post()
  async create(@Body() createOrderDto: any): Promise<{ data: Order }> {
    const order = await this.ordersService.create(createOrderDto);
    return { data: order };
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() updateOrderDto: any): Promise<{ data: Order }> {
    const order = await this.ordersService.update(id, updateOrderDto);
    return { data: order };
  }

  @Get()
  async findAll(): Promise<{ data: Order[] }> {
    const orders = await this.ordersService.findAll();
    return { data: orders };
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<{ data: Order }> {
    const order = await this.ordersService.findOne(id);
    return { data: order };
  }

  @Get('analytics/total-sold-last-month')
  async getTotalSoldLastMonth(): Promise<{ data: number }> {
    const total = await this.ordersService.getTotalSoldLastMonth();
    return { data: total };
  }

  @Get('analytics/highest-amount-order')
  async getHighestAmountOrder(): Promise<{ data: Order }> {
    const order = await this.ordersService.getHighestAmountOrder();
    return { data: order };
  }
}
